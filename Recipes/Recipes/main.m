//
//  main.m
//  Recipes
//
//  Created by Richard on 12/31/12.
//  Copyright (c) 2012 Guilford College. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
