//
//  AppDelegate.h
//  Recipes
//
//  Created by Richard on 12/31/12.
//  Copyright (c) 2012 Guilford College. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
